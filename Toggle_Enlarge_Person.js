// CONFIGURATION
// If one or more tokens are selected, those will be used instead of the listed actors
// Leave the actorNames array empty to guess the players
// Example actorNames: `actorNames: ["Bob", "John"],`
const c = {
  actorNames: [],
  buffName: "Enlarge Person"
};
// END CONFIGURATION

const transform = (caster, size) => {
  caster.update({'data.traits.size': size});
};

const sizes = ["fine", "dim", "tiny", "sm", "med", "lg", "huge", "grg", "col"];

var sizeToBe = 0

const tokens = canvas.tokens.controlled;
let actors = tokens.map(o => o.actor);
if (!actors.length && c.actorNames.length) actors = game.actors.entities.filter(o => c.actorNames.includes(o.name));
if (!actors.length) actors = game.actors.entities.filter(o => o.isPC);
actors = actors.filter(o => o.hasPerm(game.user, "OWNER"));

if (!actors.length) ui.notifications.warn("No applicable actor(s) found");
else {
  for (let actor of actors) {
    const buff = actor.items.find(o => o.name === c.buffName && o.type === "buff");
    if (buff != null) {
      let active = getProperty(buff.data, "data.active");
      if (active == null) active = false;
      for (let tester in sizes) {
        if (actor.data.data.traits.size == sizes[tester]) {
          var currentSize = parseInt(tester);
          break;
        }
      }
      if (active == false) sizeToBe = currentSize + 1;
      else sizeToBe = currentSize - 1;
      transform(actor, sizes[sizeToBe]);
      buff.update({ "data.active": !active });
    }
  }
}